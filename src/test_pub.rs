use std::thread;
use std::time::Duration;

use dsp_tools::io::*;
use num::Complex;
use waterfall::DataGenerator;

fn main() {
    let host = "127.0.0.1:5678";
    let mut tcp_tx = TcpSender::new(host).expect("failed to create TcpStream at {host:?}");
    let mut gen = DataGenerator::new(512, 200.0);

    loop {
        let cpx_vec: Vec<Complex<f32>> = gen.fake_data_gen();
        let packet = IQPacket::<Complex<f32>>::new(1, 2, &cpx_vec);
        tcp_tx.send(&packet).expect("failed to send packet");
        thread::sleep(Duration::from_millis(1));
    }
}
