use num::Complex;
use num::Zero;

pub struct DataGenerator {
    t: f32,
    nfft: usize,
    f: f32,
}

impl DataGenerator {
    pub fn new(nfft: usize, freq: f32) -> DataGenerator {
        DataGenerator {
            t: 0.0,
            nfft: nfft,
            f: freq,
        }
    }

    pub fn fake_data_gen(&mut self) -> Vec<Complex<f32>> {
        use rand_distr::StandardNormal;
        use rand::prelude::*;
        use rand::rngs::SmallRng;

        let mut data = vec![Complex::new(0.0, 0.0); self.nfft];
        let mut last = Complex::zero();
        let mut lastlast = Complex::zero();
        for j in 0..self.nfft {
            // Generate colored AWGN with a real valued sinusoidal term
            let v1: f32 = SmallRng::from_entropy().sample::<f32, StandardNormal>(StandardNormal) as f32;
            let v2: f32 = SmallRng::from_entropy().sample::<f32, StandardNormal>(StandardNormal) as f32;
            self.t += 0.001;
            data[j] = Complex::new((2.0 * std::f32::consts::PI * self.f * self.t).cos(), 0.0)
                + Complex::new(v1, v2);
            data[j] += lastlast + last;
            data[j] /= 3.0;
            lastlast = last;
            last = data[j];
        }

        data
    }
}
