extern crate serde;

extern crate serde_derive;

#[macro_use]
extern crate glium;
extern crate image;
extern crate rustfft;

use std::collections::VecDeque;

use dsp_tools::io::*;

use num::Complex;
use rustfft::FftPlanner;

fn main() {
    println!("Starting Waterfall...");
    let waterfall = Waterfall::new(512);
    draw_waterfall(waterfall);
}

struct Waterfall {
    nfft: usize,
    buffer: VecDeque<Vec<f32>>,
}

impl Waterfall {
    fn new(nfft: usize) -> Waterfall {
        Waterfall {
            nfft: nfft,
            buffer: VecDeque::new(),
        }
    }

    fn push(&mut self, data: Vec<f32>) {
        if data.len() != self.nfft {
            panic!("Tried to push size other than nfft = {}", self.nfft);
        }
        self.buffer.push_back(data);
    }
}

fn draw_waterfall(mut waterfall: Waterfall) {
    use glium::{glutin, Surface};

    // Window and context setup
    let events_loop = glutin::event_loop::EventLoop::new();
    let window = glutin::window::WindowBuilder::new()
        .with_title("Waterfall Demo")
        .with_inner_size(glutin::dpi::LogicalSize::new(waterfall.nfft as f64, 512.0));
    let context = glutin::ContextBuilder::new();
    let display = glium::Display::new(window, context, &events_loop).unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let vbuf = glium::vertex::VertexBuffer::new(
        &display,
        &[
            Vertex {
                position: [-1.0, 1.0],
                tex_coords: [0.0, 1.0],
            },
            Vertex {
                position: [1.0, 1.0],
                tex_coords: [1.0, 1.0],
            },
            Vertex {
                position: [-1.0, -1.0],
                tex_coords: [0.0, 0.0],
            },
            Vertex {
                position: [1.0, -1.0],
                tex_coords: [1.0, 0.0],
            },
        ],
    )
    .unwrap();

    let vertex_shader_src = r#"
        #version 150

        in vec2 position;
        in vec2 tex_coords;

        out vec2 v_tex_coords;

        void main() {
            v_tex_coords = tex_coords;
            gl_Position = vec4(position, 0.0, 1.0);
        }
    "#;

    let fragment_shader_src = r#"
        #version 150

        in vec2 v_tex_coords;

        out vec4 color;

        uniform sampler2D diffuse_tex;

        void main() {
            color = texture(diffuse_tex, v_tex_coords);
        }
    "#;

    // Compile shaders
    let program =
        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None)
            .unwrap();

    // Create FFT plan
    let mut planner = FftPlanner::new();
    let fft = planner.plan_fft_forward(waterfall.nfft);

    // Create fake data generator
    let host = "127.0.0.1:5678";
    let mut tcp_rx = TcpRx::new(host).expect("failed to create listener at host {host:?}");

    let height = 512;
    let mut max_val: f32 = 0.0;
    for mut packet in tcp_rx.listen::<Complex<f32>>().expect("failed to listen()") {
        let mut target = display.draw();
        target.clear_color(0.0, 0.0, 0.0, 1.0);

        // Calculate the FFT and push the new spectrum into the Waterfall
        fft.process(&mut packet.data);
        for i in 0..waterfall.nfft / 2 {
            let temp = packet.data[i];
            packet.data[i] = packet.data[i + waterfall.nfft / 2];
            packet.data[i + waterfall.nfft / 2] = temp;
        }
        let mut spectrum: Vec<f32> = packet.data
            .iter()
            .map(|x| 10.0 * (x.norm_sqr()).log10())
            .collect();
        let new_max_val = spectrum.iter().cloned().fold(-1.0 / 0.0, f32::max);
        if new_max_val > max_val {
            max_val = new_max_val;
        }
        spectrum = spectrum.iter().map(|x| x / max_val).collect();
        waterfall.buffer.push_back(spectrum);
        if waterfall.buffer.len() > height {
            waterfall.buffer.pop_front();
        }

        let mut image: Vec<Vec<(f32, f32, f32)>> = Vec::new();
        for row in waterfall.buffer.iter() {
            let mut newrow = Vec::new();
            for &val in row {
                newrow.push((val, val, val));
            }
            image.push(newrow);
        }
        let diffuse_texture = glium::texture::Texture2d::new(&display, image).unwrap();

        // Draw it
        target
            .draw(
                &vbuf,
                glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip),
                &program,
                &uniform! {diffuse_tex: &diffuse_texture},
                &Default::default(),
            )
            .unwrap();
        target.finish().unwrap();
    }
}
