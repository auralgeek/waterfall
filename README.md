Waterfall
=========

Waterfall visualization using Rust and OpenGL for data coming in over a ZMQ
publisher.

![Fake_Data](doc/screenshot.png)

Installation
------------

Requires OpenGL libraries to be installed (which depends completely on your
graphics card or lack thereof), as well as ZMQ.  On Ubuntu:

``` sh
$ sudo apt install libzmq3-dev
$ cargo run --release
```
